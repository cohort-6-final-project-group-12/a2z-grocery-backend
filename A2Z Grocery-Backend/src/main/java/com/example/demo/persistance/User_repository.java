package com.example.demo.persistance;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.User;

public interface User_repository extends JpaRepository<User, Integer> {

	public User findByEmail(String email);

}
