package com.example.demo.persistance;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Cart;

public interface Cart_repository extends JpaRepository<Cart, Integer> 
{
	@Query("SELECT c FROM Cart c WHERE " +"c.pid=:query")
	List<Cart> displayCartById(@Param("query") int query);
	
	@Query("SELECT c FROM Cart c WHERE " +"c.uid=:query")
	List<Cart> displayCartByUid(@Param("query") int query);
	
	@Modifying
	@Query("DELETE FROM Cart c WHERE c.uid in ?1")
	void delCartByUid(int uid);
	
//	@Query("UPDATE Cart c "+ "SET c.pcount=c.pcount+1 WHERE c.pid = :query")
//	void UpdateCount(@Param("query") int query);
}
