package com.example.demo.persistance;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.ProductCategory;

public interface Product_category_repository extends JpaRepository<ProductCategory, Integer> {

}
