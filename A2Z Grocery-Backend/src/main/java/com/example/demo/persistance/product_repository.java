package com.example.demo.persistance;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Product;

public interface product_repository extends JpaRepository<Product, Integer>
{
	@Query("SELECT p FROM Product p WHERE " + "p.pname LIKE CONCAT('%',:query, '%')")
	List<Product> searchProducts(String query);

	@Query("SELECT p FROM Product p WHERE " + "p.pcid=:query")
	List<Product> getByPcid(@Param("query") int query);

}
