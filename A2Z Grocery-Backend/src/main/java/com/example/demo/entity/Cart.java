package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cart")
public class Cart {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cid")
	private int cid;
	@Column(name = "uid")
	private int uid;
	@Column(name = "pid")
	private int pid;
	@Column(name = "pprice")
	private double pprice;
	@Column(name = "pimage")
	private String pimage;
	@Column(name = "pname")
	private String pname;

	public Cart() 
	{
		// TODO Auto-generated constructor stub
	}

	public Cart(int cid, int uid, int pid, double pprice,String pimage,String pname) 
	{
		this.cid = cid;
		this.uid = uid;
		this.pid = pid;
		this.pprice = pprice;
		this.pimage=pimage;
		this.pname=pname;
	}

	public double getPprice() {
		return pprice;
	}

	public void setPprice(double pprice) {
		this.pprice = pprice;
	}

	public String getPimage() {
		return pimage;
	}

	public void setPimage(String pimage) {
		this.pimage = pimage;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}


	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	@Override
	public String toString() {
		return "Cart [cid=" + cid + ", uid=" + uid + ", pid=" + pid + ", totalprice=" + pprice + ", pimage="
				+ pimage + ", pname=" + pname + "]";
	}

}