package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pid")
	private int pid;
	@Column(name = "pimage")
	private String pimage;
	@Column(name = "pname")
	private String pname;
	@Column(name = "pdesc")
	private String pdesc;
	@Column(name = "pprice")
	private double pprice;
	@Column(name = "pcid")
	private int pcid;

	public Product() 
	{
		
	}

	public Product(int pid, String pimage, String pname, String pdesc, double pprice, int pcid) {
		this.pid = pid;
		this.pimage = pimage;
		this.pname = pname;
		this.pdesc = pdesc;
		this.pprice = pprice;
		this.pcid = pcid;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getPimage() {
		return pimage;
	}

	public void setPimage(String pimage) {
		this.pimage = pimage;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getPdesc() {
		return pdesc;
	}

	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}

	public double getPprice() {
		return pprice;
	}

	public void setPprice(double pprice) {
		this.pprice = pprice;
	}

	public int getPcid() {
		return pcid;
	}

	public void setPcid(int pcid) {
		this.pcid = pcid;
	}

	@Override
	public String toString() {
		return "Product [pid=" + pid + ", pimage=" + pimage + ", pname=" + pname + ", pdesc=" + pdesc + ", pprice="
				+ pprice + ", pcid=" + pcid + "]";
	}
}
