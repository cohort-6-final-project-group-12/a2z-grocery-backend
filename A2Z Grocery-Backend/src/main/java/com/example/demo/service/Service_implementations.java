package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Product;
import com.example.demo.entity.ProductCategory;
import com.example.demo.entity.User;
import com.example.demo.entity.Cart;
import com.example.demo.persistance.Cart_repository;
import com.example.demo.persistance.Product_category_repository;
import com.example.demo.persistance.User_repository;
import com.example.demo.persistance.product_repository;

@Service
public class Service_implementations 
{	
	@Autowired
	private Product_category_repository productCategoryRepo;
	
	@Autowired
	private product_repository productRepo;
	
	@Autowired
	private Cart_repository cartRepo;
	
	@Autowired
	private User_repository userRepo;
	
	PasswordEncoder passwordEncoder;

	public Service_implementations(Product_category_repository productCategoryRepo, product_repository productRepo, Cart_repository cartRepo,
			User_repository userRepo) {
		this.productCategoryRepo = productCategoryRepo;
		this.productRepo = productRepo;
		this.cartRepo = cartRepo;
		this.userRepo = userRepo;
		passwordEncoder=new BCryptPasswordEncoder();
	}

	@Transactional
	public List<Product> displayAll() {
		return productRepo.findAll();
	}

	@Transactional
	public List<ProductCategory> displayAllC() {
		return productCategoryRepo.findAll();
	}

	@Transactional
	public void insert(Product p) {
		productRepo.save(p);
	}

	@Transactional
	public void update(Product p) {
		productRepo.save(p);
	}

	@Transactional
	public void delete(int id) {
		productRepo.deleteById(id);
	}

	@Transactional
	public void insertPC(ProductCategory pc) {
		productCategoryRepo.save(pc);
	}

	@Transactional
	public void updatePC(ProductCategory pc) {
		productCategoryRepo.save(pc);
	}

	@Transactional
	public void deletePC(int id) {
		productCategoryRepo.deleteById(id);
	}

	@Transactional
	public List<Product> searchProducts(String name) {
		return productRepo.searchProducts(name);
	}

	@Transactional
	public List<Cart> displayCart() {
		return cartRepo.findAll();
	}

	@Transactional
	public List<Product> displayPById(int id) {
		ProductCategory pc = productCategoryRepo.findById(id).get();
		return productRepo.getByPcid(pc.getPcid());
	}

	@Transactional
	public void addCart(Cart c) {
		cartRepo.save(c);
	}

	@Transactional
	public void updateCart(Cart c) {
		cartRepo.save(c);
	}

	@Transactional
	public void delCart(int id) {
		cartRepo.deleteById(id);
	}

	@Transactional
	public List<User> displayUser() {
		return userRepo.findAll();
	}
	
	@Transactional
	public User displayUserByEmail(String email) {
		return userRepo.findByEmail(email);
	}

	@Transactional
	public void insertUser(User u) 
	{
		String encodedPassword=this.passwordEncoder.encode(u.getPassword());
		u.setPassword(encodedPassword);
		userRepo.save(u);
	}

	@Transactional
	public void updateUser(User u) 
	{
//		String encodedPassword=this.passwordEncoder.encode(u.getPassword());
//		u.setPassword(encodedPassword);
		userRepo.save(u);
	}

	@Transactional
	public void deleteUser(int id) {
		userRepo.deleteById(id);
	}

	@Transactional
	public User login(String email, String password)
	{
		User user = userRepo.findByEmail(email);
		String uenpwd=user.getPassword();
		if(passwordEncoder.matches(password, uenpwd))
		{
			return user;
		}else
		{
			return null;
		}
		
	}
	
	@Transactional
	public List<Cart> displayCByPId(int id)
	{
		return cartRepo.displayCartById(id);
	}
	
	@Transactional
	public List<Cart> displayCByUid(int uid)
	{
		return cartRepo.displayCartByUid(uid);
	}
	
	@Transactional
	public Product displayPByPId(int id)
	{
		return productRepo.findById(id).get();
	}
	
	@Transactional
	public void delCartByUid(int uid) 
	{
		cartRepo.delCartByUid(uid);
	}
	
//	@Transactional
//	public void updateCount(int query)
//	{
//		cartRepo.UpdateCount(query);
//	}

}
