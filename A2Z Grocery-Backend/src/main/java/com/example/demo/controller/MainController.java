package com.example.demo.controller;

import java.math.BigInteger;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Product;
import com.example.demo.entity.ProductCategory;
import com.example.demo.entity.User;
import com.example.demo.payment.order_request;
import com.example.demo.payment.order_response;
import com.example.demo.entity.Cart;
import com.example.demo.service.Service_implementations;
import com.razorpay.Order;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class MainController {
	private RazorpayClient client;
	private static final String SECRET_ID1 = "rzp_test_JEV5T9voaX0yzl";
	private static final String SECRET_KEY1 = "PEY6PNEK2T4hOkSyYOEQwZOs";
	@Autowired
	private Service_implementations serviceImpl;
	public MainController(Service_implementations serviceImpl) {
		super();
		this.serviceImpl = serviceImpl;
	}

//public List<Product> displayAll(@PathVariable("pcid") int pid) 
//{
//	return serviceImpl.displayAll(pid);
//}

	@RequestMapping(path = "/createOrder", method = RequestMethod.POST)
	public order_response createOrder(@RequestBody order_request orderRequest) {
		order_response response = new order_response();
		try {

			if (orderRequest.getAmount().intValue() > 0) {
				client = new RazorpayClient(SECRET_ID1, SECRET_KEY1);
			} 

			Order order = createRazorPayOrder(orderRequest.getAmount());
			System.out.println("---------------------------");
			String orderId = (String) order.get("id");
			System.out.println("Order ID: " + orderId);
			System.out.println("---------------------------");
			response.setRazorpayOrderId(orderId);
			response.setApplicationFee("" + orderRequest.getAmount());
			if (orderRequest.getAmount().intValue() > 0) {
				response.setSecretKey(SECRET_KEY1);
				response.setSecretId(SECRET_ID1);
				response.setPgName("razor1");
			}

			return response;
		} catch (RazorpayException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return response;

	}

	private Order createRazorPayOrder(BigInteger amount) throws RazorpayException {

		JSONObject options = new JSONObject();
		options.put("amount", amount.multiply(new BigInteger("100")));
		options.put("currency", "INR");
		options.put("receipt", "txn_123456");
		options.put("payment_capture", 1); // You can enable this if you want to do Auto Capture.
		return client.orders.create(options);
	}
	
	// display all products
	@GetMapping("/displayP")
	public List<Product> displayAll() {
		return serviceImpl.displayAll();
	}

	// display all product categories
	@GetMapping("/displayPC")
	public List<ProductCategory> display() {
		return serviceImpl.displayAllC();
	}

	// add product
	@PostMapping("/displayP")
	public void insert(@RequestBody Product p) {
		serviceImpl.insert(p);
	}

	// update product
	@PutMapping("/displayP")
	public void update(@RequestBody Product p) {
		serviceImpl.update(p);
	}

	// delete product
	@DeleteMapping("/displayP/{pid}")
	public void delete(@PathVariable("pid") int id) {
		serviceImpl.delete(id);
	}

	// add product category
	@PostMapping("/displayPC")
	public void insertPC(@RequestBody ProductCategory pc) {
		serviceImpl.insertPC(pc);
	}

	// update product category
	@PutMapping("/displayPC")
	public void updatePC(@RequestBody ProductCategory pc) {
		serviceImpl.updatePC(pc);
	}

	// delete product category
	@DeleteMapping("/displayPC/{pcid}")
	public void deletePC(@PathVariable("pcid") int id) {
		serviceImpl.deletePC(id);
	}

	// get product by name ( search )
	@GetMapping("/search/{name}")
	public List<Product> searchProducts(@PathVariable("name") String name) {
		return serviceImpl.searchProducts(name);
	}

	// display all carts
	@GetMapping("/displayCart")
	public List<Cart> displaycart() {
		return serviceImpl.displayCart();
	}

	// add new cart
	@PostMapping("/displayCart")
	public void addToCart(@RequestBody Cart c) {
		serviceImpl.addCart(c);
	}

	// update cart
	@PutMapping("/displayCart")
	public void updateCart(@RequestBody Cart c) {
		serviceImpl.addCart(c);
	}

	// delete cart by cart id
	@DeleteMapping("/displayCart/{cid}")
	public void delCart(@PathVariable("cid") int cid) {
		serviceImpl.delCart(cid);
	}
	
	@DeleteMapping("/displayCartByUid/{uid}")
	public void delCartByUid(@PathVariable("uid") int uid) {
		serviceImpl.delCartByUid(uid);
	}
	// display user
	@GetMapping("/displayUser")
	public List<User> displayUser() {
		return serviceImpl.displayUser();
	}
	
	//find user by Email to write for checking user exist or not
	@GetMapping("/displayUser/{email}")
	public User displayUserByEmail(@PathVariable("email") String email)
	{
		return serviceImpl.displayUserByEmail(email);
	}
	
	// check user login
	@GetMapping("/login/{email}/{password}")
	public User login(@PathVariable("email") String email, @PathVariable("password") String password) {
		return serviceImpl.login(email,password);
	}

	// add user
	@PostMapping("/displayUser")
	public void insertUser(@RequestBody User u) {
		serviceImpl.insertUser(u);
	}

	// update user
	@PutMapping("/displayUser")
	public void updateUser(@RequestBody User u)
	{
		serviceImpl.updateUser(u);
	}

	// delete user
	@DeleteMapping("displayUser/{id}")
	public void deleteUser(@PathVariable("id") int id) {
		serviceImpl.deleteUser(id);
	}

	// display product by their category id
	@GetMapping("/displayP/{pcid}")
	public List<Product> displayPById(@PathVariable("pcid") int pid) {
		return serviceImpl.displayPById(pid);
	}
	
	// display products by pid
	@GetMapping("/Product/{id}")
	public Product displayP(@PathVariable("id") int pid)
	{
		return serviceImpl.displayPByPId(pid);
	}
	
	// display cart by pid
//	@GetMapping("/Cart/{pid}")
//	public List<Cart> displayCartByPid(@PathVariable("pid") int pid)
//	{
//		return serviceImpl.displayCByPId(pid);
//	}
//	
	// display cart by uid
	@GetMapping("/Cart/{uid}")
	public List<Cart> displayCartByUid(@PathVariable("uid") int uid)
	{
		return serviceImpl.displayCByUid(uid);
	}
	
	// update pcount
//	@PutMapping("/UpdateCount")
//	public void updateCount(@RequestBody Cart c)
//	{
//		serviceImpl.updateCount(c.getPid());
//	}	
}