insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(101,1,"refreshment soda","assets/images/1001.jpg","Thumpsup",60);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(102,1,"reenergize tea","assets/images/1002.jpg","Red label",45);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(103,1,"mango juice","assets/images/1003.jpg","Real Mango",70);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(104,1,"chill Out","assets/images/1004.jpg","Sprite",70);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(105,1,"real shakes","assets/images/1005.jpg","Cavin's Milkshake",40);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(201,2,"potato chips","assets/images/2001.jpg","lays",37);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(202,2,"Cream Biscuits","assets/images/2002.jpg","Oreo",30);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(203,2,"Dark Chocolate","assets/images/2003.jpg","DairyMilkSilk",85);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(204,2,"Casatta","assets/images/2004.jpg","Arun's IceCream",65);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(205,2,"Indian Sweets","assets/images/2005.jpg","Daadu's Laddu",100);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(301,3,"Fabric Conditioners","assets/images/3001.jpg","Comfort",100);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(302,3,"Washroom Cleaner","assets/images/3002.jpg","Harpic",180);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(303,3,"Air Refreshners","assets/images/3003.jpg","Godrej aer Spray",160);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(304,3,"Dishwash detergents","assets/images/3004.jpg","Vim",70);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(305,3,"Washing Powder","assets/images/3005.jpg","SurfExcel",85);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(401,4,"Rice Bran Oil","assets/images/4001.jpg","Freedom Refined Oil",300.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(402,4,"Multigrain Aata","assets/images/4002.jpg","Ashirwad Aata",450.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(403,4,"Classic Masala Oats","assets/images/4003.jpg","Saffola Masala Oats",186.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(404,4,"Moons & stars","assets/images/4004.jpg","Kellogg's Chocos",425.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(405,4,"Breakfast Mix","assets/images/4005.jpg","MTR Upma",85.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(501,5,"Skin Protecting Jelly","assets/images/5001.jpg","Vaseline",394.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(502,5,"RichCreme Hair Color","assets/images/5002.jpg","Godrej Expert",150.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(503,5,"Cool Shower Gel","assets/images/5003.jpg","Nivea Men",185.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(504,5,"Skincare Gift Kit","assets/images/500.jpg","mCaffeine",1425.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(505,5,"Aloe Vera Gel","assets/images/5005.jpg","MamaEarth",287.00);

insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(106,1,"Energy Drink Can","assets/images/1006.jpg","Red Bull",243.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(107,1,"Mixed Fruit Juice","assets/images/1007.jpg","Real",450.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(108,1,"Vanilla ThickShake","assets/images/1008.jpg","Jersey",45.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(109,1,"Rich Fruit Juice","assets/images/1009.jpg","Paper Boat",50.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(110,1,"Energy Drink","assets/images/1010.jpg","Mountain Dew",300.00);

insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(206,2,"Cripsy & Crunchy","assets/images/2006.jpg","Little Hearts",40.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(207,2,"Hot & Spicy","assets/images/2007.jpg","Haldiram's",145.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(208,2,"Chicken Nuggets","assets/images/2008.jpg","Itc Master Chef",350.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(209,2,"Potato Chips","assets/images/2009.jpg","Pringles",300.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(210,2,"Cream & Onion","assets/images/2010.jpg","Mr Makhana",50.00);

insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(307,3,"Room Freshner","assets/images/3007.jpg","Odonil",118.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(308,3,"Garbage Bags","assets/images/3008.jpg","Beco",300.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(309,3,"Liquid Vapouriser","assets/images/3009.jpg","Good Knight",450.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(310,3,"Facial Tissues","assets/images/3010.jpg","Paseo",300.00);

insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(407,4,"Premium Eggs","assets/images/4007.jpg","Dr.Good Eggs",100.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(408,4,"Garam Masala","assets/images/4008.jpg","Everest",50.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(409,4,"Tomato Pickle","assets/images/4009.jpg","Priya",250.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(410,4,"Mayonnaise","assets/images/4010.jpg","Dr.Oetker",240.00);

insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(509,5,"Soft & Fresh ","assets/images/5009.jpg","Pears",110.00);
insert into product(pid,pcid,pdesc,pimage,pname,pprice) values(510,5,"Intense Moisturizer","assets/images/5010.jpg","Lakme",398.00);